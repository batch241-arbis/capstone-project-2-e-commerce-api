const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController");
const auth = require("../auth");

// Create a product Route (needs admin auth)
router.post("/addproduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization).isAdmin;
	productController.addProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
});

// Retrieve active products Route
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) => {
	productController.retrieveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.post("/update/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/archive/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(userData, req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;