const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController");
const auth = require("../auth");

// Register user Route
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Login user Route
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Add order Route
router.post("/order", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const orderDetails = {
		product: req.body.product,
		quantity: req.body.quantity
	}
	userController.addOrder(userData, orderDetails).then(resultFromController => res.send(resultFromController));
});

// Retrieve user details Route
router.get("/details/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getDetails(req.params).then(resultFromController => res.send(resultFromController));
});

// STRETCH GOALS
// Set user as admin route
router.get("/setadmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization).isAdmin;
	userController.setAdmin(userData, req.body).then(resultFromController => res.send(resultFromController));
});

// Retrieve authenticated user's order route
router.get("/getorder/:id", auth.verify, (req, res) => {
	userController.getOrder(req.params).then(resultFromController => res.send(resultFromController));
});

// Retrieve all orders controller (Admin auth needed)
router.get("/getallorder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrder(userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;