// Application dependencies
const express = require("express");
const mongoose = require("mongoose");
const userRoute = require("./Routes/userRoute")
const productRoute = require("./Routes/productRoute")
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.eua2rv1.mongodb.net/Capstone-Project-2-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then().catch(err => console.log(err));

app.use("/users", userRoute);
app.use("/products", productRoute);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));

app.listen(port, () => console.log(`API is now online on port ${port}`));