const Product = require("../Models/Products");

// Add product controller (Needs admin auth)
module.exports.addProduct = (adminStatus, reqBody) => {
	if (adminStatus == true) {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
		})
		return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return product
			};
		});
	} else {
		return Promise.reject('Not authorized to access this page');
	}
};

// Retrieve all active products controller
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve single product by id
module.exports.retrieveProduct = (reqParams) => {
	console.log(reqParams.id)
	return Product.findById(reqParams.id, {_id: 0}).then(result => {
		return result;
	})/*.catch(err => console.log(err));*/
};

module.exports.updateProduct = (adminStatus, reqParams, reqBody) => {
	if (adminStatus == true) {
		let updateProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			orders: {
				orderId: reqBody.orderId
			}
		};
		console.log(reqParams.id)
	return Product.findByIdAndUpdate(reqParams.id, updateProduct).then(result => {
		return result;
	}).catch(err => console.log(err));
	} else {
		return Promise.reject('Not authorized to access this page');
	}
};

// Archiving a product by id
module.exports.archiveProduct = (adminStatus, reqParams) => {
	if (adminStatus == true) {
	return Product.findByIdAndUpdate(reqParams.id, {isActive: false}).then(result => {
			return result;
		}).catch(err => console.log(err));
	} else {
		return Promise.reject('Not authorized to access this page');
	};
};


// Update single product by id (Needs admin auth)
// module.exports.updateProduct = ()