const User = require("../Models/Users");
const Product = require("../Models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register user controller
module.exports.registerUser = (reqBody) => {
	let userData = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return userData.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user;
		};
	});
};

// Login user controller
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			message = "Please use a registered email."
			return message
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return ({auth: "Email / Password is incorrect."})
			}
		}
	})
}

// Add order controller
/*module.exports.addOrder = (userData, reqBody) => {
	if (userData.isAdmin == false) {
		let newOrder = ({
				products: { 
					productName: reqBody.productName,
					quantity: reqBody.quantity
				},
				totalAmount: reqBody.totalAmount
		})
		console.log(newOrder)
		return User.findByIdAndUpdate(userData.id, {$addToSet:{orders: newOrder}}).then(result => {
			return result.orders;
		})
	} else {
		return Promise.reject('Not authorized to access this page');
	}
}*/

module.exports.addOrder = async (userData, orderDetails) => {
	if (!userData.isAdmin) {
		let userOrderChanged = await User.findById(userData.id, {orders: true}).then(user => {
			user.orders.push({
				products:{
					productName: orderDetails.product, 
					quantity: orderDetails.quantity
				}
			})
			return user.save().then(result => {
				return result.orders[result.orders.length-1].id
			})
		}).catch(err => console.log(err));
		let productDetailsChanged = await Product.findOne({name: orderDetails.product}).then(product => {
			product.orders.push({orderId: userOrderChanged})
			return product.save().then(result => {
				return true
			}).catch(err => console.log(err));
		})
		if (userOrderChanged && productDetailsChanged){
			let message = {message: "Order successfully added."}
			return message
		} else {
			let message ={message: "Order failed to be added."}
			return message
		}
	}
}

// Get user details controller
module.exports.getDetails = (reqParams) => {
	return User.findById(reqParams.id).then(result => {
		return result;
	})
}

// STRETCH GOALS
// Set user as admin controller
module.exports.setAdmin = (adminStatus, reqBody) => {
	if (adminStatus == true) {
		return User.findByIdAndUpdate(reqBody.id, {isAdmin: true}).then(result => {
			return result;
		})
	} else {
		return Promise.reject('Not authorized to access this page');
	}
};

// Retrieve authenticated user's orders controller
module.exports.getOrder = (reqParams) => {
	return User.findById(reqParams.id, {orders: true}).then(result => {
		return result;
	});
};

// Retrieve all orders controller (Admin auth needed)
module.exports.getAllOrder = (adminStatus) => {
	if (adminStatus == true) {
		User.find({}, {orders: true}).then(result => {
			return result;
		});
	}
}


/*module.exports.createOrder = (reqParams, reqBody) => {
	let userData = User.findById(reqParams.userid)
	let productData: Product.findOne({name: reqBody.product})
	let newOrder = new Order({
		userId: userData,
		products: {
		product: productData.productId,
		quantity: reqBody.quantity
		}
		totalAmount: reqBody.totalAmount
	});
	return Order.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return user
		};
	}
}*/